INTRODUCTION
------------

Material Admin Theme is a Responsive, beautiful
and very functional admin theme for Drupal 8 based on
Google Material Design principles: material.google.com.
It aims to bring clean and simple UX interface to help
Drupal administrators and content managers manage their
sites easier. See https://www.agnian.com/drupal-material-interface
for more information about the theme. To learn how to build
your own custom theme and override Drupal's default code, 
see the Theming Guide: www.drupal.org/docs/8/theming.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

    1. Download the theme from www.drupal.org/project/material.
    2. Place the plugin in the root themes folder (/themes) or
       /admin/appearance click install new theme.
    3. /admin/appearance find and install the theme.

CONFIGURATION
-------------

    1. Check Block layout (admin/structure/block/list/material)
       Material Admin, fix blocks. It means tabs, page title,
       main page content, primary admin actions should be in Content region.
    2. /admin/appearance choose the theme as administration theme.
